Automation database (influxdb)
==============================

The entire automation framework relies on a database to save all the relevant workflows/jobs configurations and status.
The influxdb that support the framework is hosted by the CERN IT on the [dbod platform](https:dbod.web.cern.ch). The original request for the database creation can be found in this [ticket](https://cern.service-now.com/service-portal?id=ticket&table=e&n=RQF1758809). The ticket contains useful links to the dbod user guide. The influxdb instance has been setup to perform daily backups.
