#!/usr/bin/env python3
import sys
import os
import shutil
import subprocess
import numpy as np
from multiprocessing import Pool
from typing import Optional, Dict, List, Any
from os import remove, path
from ecalautoctrl import HandlerBase, JobCtrl, process_by_run, prev_task_data_source
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

class AlignMediateHandler(HandlerBase):
    """
    Run all intermediate steps of ECAL alignment.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        if not deps_tasks:
            deps_tasks = [prev_input]
        else:
            deps_tasks.append(prev_input)
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

    def merge_files(self, outfilepath: str):
        """
        Merge alignment txt files. There is one txt file per SM/Dee. 
        Merge these to one txt for EB and EE.

        :param outfilepath: path to the eos directory containing the txt files        
        """
        ret = subprocess.run(f'source {os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/merge_txt_files.sh {outfilepath}',
                                        shell=True, capture_output=True)                         
    
    def get_current_tag(self, gt, outfilepath: str):
        """
        Gets the current ECAL alignment tag from the GT and converts it into 2 txt files (one for EB and another for EE).  
       
        :param gt: current global tag
        :param outfilepath: path to the eos directory containing the txt files        
        """
        ret = subprocess.run(f'python3 {os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/sqlite_to_txt.py {gt}',
                                         shell=True, capture_output=True)
                             
        
    def process_data(self, groups: List[Dict], resubmit: bool=False):
        """
        Execute the following steps:
        - merging alignment txt files 
        - create a similar txt file from the current alignment tag 
        - Combine the new alignment conditions with the current one
        - Create sqlite files 
        
        :param groups: groups of run to be processed.
        :param resubmit: resubmission of failed jobs.
        """

        for group in groups:
            self.log.info('Processing run(s): '+','.join([r['run_number'] for r in group]))
            # reset output
            outputs = {}
            # gather master run details
            mrun = group[-1]
            run = mrun['run_number']
            fill = mrun['fill']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run, 'fill' : mrun['fill']},
                            dbname=self.opts.dbname)
            if not jctrl.taskExist():                
                jctrl.createTask(jids=[0],
                                 fields=[{'group' : ','.join([r['run_number'] for r in group[:-1]])}])
            try:
                # set run status
                self.rctrl.updateStatus(run=run, status={self.task : 'processing'})
                for r in group[:-1]:
                    self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'merged'})
                jctrl.running(jid=0)
                # get files
                files = self.get_files(group)
                self.log.info(files)
                # set ouput name
                eospath = path.abspath(path.dirname(list(files)[0]))
                # merge single job / single runs out files
                self.merge_files(outfilepath=eospath)
                # get the current alignment tag and convert it to another txt file
                global_tag = self.getField('alignment-mediate', field='global tag', runs=run, fills=None, era=None)
                self.get_current_tag(gt=global_tag, outfilepath=eospath)
                # combine alignment txt files
                ret_combine_EB = subprocess.run(f'CombineRotoTraslations {eospath}/EB_alignment_coeff_old.txt {eospath}/EBAlignmentCoefficients_merged.txt {eospath}/EBAlignment_Run3_NewConditions.txt',
                                        cwd=f'{os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/'
                                        shell=True, capture_output=True)

                ret_combine_EE = subprocess.run(f'CombineRotoTraslations {eospath}/EE_alignment_coeff_old.txt {eospath}/EEAlignmentCoefficients_merged.txt {eospath}/EEAlignment_Run3_NewConditions.txt',
                                        cwd=f'{os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/'
                                        shell=True, capture_output=True)
                
                # create sqlite files (new alignment conditions) 

                ret_newtag_EB = subprocess.run(f'cmsRun {os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/test/dbEcalAlignment/copyFileAlignEB_cfg.py inFile={eospath}/EBAlignment_Run3_NewConditions.txt',
                                         shell=True, capture_output=True)
                shutil.copy('EBAlign_test.db', eospath)
                shutil.copy('EBAlign_test.db', os.path.dirname(eospath))
                os.remove('EBAlign_test.db')

                ret_newtag_EE = subprocess.run(f'cmsRun {os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/test/dbEcalAlignment/copyFileAlignEE_cfg.py inFile={eospath}/EEAlignment_Run3_NewConditions.txt',
                                         shell=True, capture_output=True)
                shutil.copy('EEAlign_test.db', eospath)
                shutil.copy('EEAlign_test.db', os.path.dirname(eospath))
                os.remove('EEAlign_test.db')

                # mark as completed
                outputs.update({'output' : eospath})
                jctrl.done(jid=0, fields=outputs)
            
            except Exception as e:
                jctrl.failed(jid=0)
                self.log.error(f'Failed processing merging step for run {run}: {e}')
        
    def submit(self):
        """
        Execute the local merging step. 
        Reprocess re-injected runs as well.
        """

        # process new runs / fill
        self.process_data(groups=self.groups())
            
        return 0
            
    def resubmit(self):
        """
        Resubmit runs with failed jobs
        """

        # get runs in status processing
        runs = self.rctrl.getRuns(status = {self.task : 'processing'})

        # check if any job failed before resubmitting
        for run_dict in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run_dict['run_number'],
                                  'fill' : run_dict['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed())>0:
                self.process_data(groups=[[run_dict]], resubmit=True)
        
        return 0

@prev_task_data_source
@process_by_intlumi(target=500)
class AlignMediateHandlerRun(AlignMediateHandler):
    """
    Run ECAL alignment steps that comes after minimization until sqlite production.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=[],
                 **kwargs):        
        super().__init__(task=task,
                         prev_input=prev_input,
                         deps_tasks=deps_tasks,
                         **kwargs)
    
if __name__ == '__main__':
    handler = AlignMediateHandlerRun(task='alignment-mediate',
                                prev_input='alignment-align')

    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(AlignMediateHandlerRun)
