Automation workflow docs: ECAL-TRK alignment
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

runreco.py
==========

.. argparse::
   :filename: ../runreco.py
   :func: get_opts
   :prog: runreco.py

runalign.py
===========

.. argparse::
   :filename: ../runalign.py
   :func: get_opts
   :prog: runalign.py
          
