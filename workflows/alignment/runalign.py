#!/usr/bin/env python3
import sys
import subprocess
from os import path
from typing import Optional, List
from ecalautoctrl import JobCtrl, RunCtrl, HTCHandler, process_by_intlumi, prev_task_data_source
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

@prev_task_data_source
@process_by_intlumi(target=400)
class HTCHandlerAlignment(HTCHandler):
    """
    Alignment submission handler. Submit 1 job per SM/Dee reading data from
    the previous reconstruction step.
    
    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 **kwargs):
        super().__init__(task=task, deps_tasks=[prev_input], **kwargs)
        self.prev_input = prev_input
    
    def submit(self):
        """
        Submit runs in status task : new.
        Resubmit runs in status task : reprocess.
        """
        
        for group in self.groups():
            # master run
            run = group[-1]
            fdict = self.get_files(group)
            if fdict is not None and len(fdict)>0:
                fdict = [f'file:{f}' for f in fdict]
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number' : run['run_number'], 'fill' : run['fill']},
                                dbname=self.opts.dbname)
                # allow reprecessing only if previous jobs are not running
                allow_repr=(run[self.task]=='reprocess')
                if jctrl.taskExist() and run[self.task]=='reprocess':
                    jobs = jctrl.getJobs()
                    for i in jobs['idle']+jobs['running']:
                        allow_repr &= (not self.check_running_job(jid=i))
                if not jctrl.taskExist() or allow_repr:
                    task = f'-w {self.task} -c {self.campaign} -t run_number:{run["run_number"]},fill:{run["fill"]} --db {self.opts.dbname}'
                    eosdir = path.abspath(self.opts.eosdir+f'/{run["run_number"]}/')
                    # split processing such that each job runs on 1 SM/Dee
                    # 0->EB (36 SM, from 0 to 35)
                    parts = [(0, i) for i in range(36)]
                    # 1->EE (4 Dees, from 1 to 4)                        
                    parts.extend([(1, i) for i in range(4)])
                    with open('args-align.txt', 'w') as ff:
                        ff.write('\n'.join([f'{p}, {sp}' for p,sp in parts]))
                    infiles = ','.join(fdict)
                    ret = subprocess.run(['condor_submit',
                                          f'{self.opts.template}',
                                          '-spool',
                                          '-queue', 'part, subpart from args-align.txt',
                                          '-append', f'arguments = "$(ClusterId).$(ProcId) $(ProcId) {infiles} \'{task}\' {eosdir} {self.opts.wdir} $(part) $(subpart)"',
                                          '-append', f'output = /eos/home-e/ecalgit/www/logs/{self.task}-$(ClusterId)-$(part)-$(subpart).log',
                                          '-append', f'error = /eos/home-e/ecalgit/www/logs/{self.task}-$(ClusterId)-$(part)-$(subpart).log'],
                                         capture_output=True)
                    #remove('args.txt')
                    if ret.returncode == 0:
                        # awfully parse condor_submit output to get the clusterId
                        self.log.info(f'Submitting jobs for run(s): '+','.join([r['run_number'] for r in group])+f' fill {run["fill"]}')
                        self.log.info(ret.stdout.decode().strip())
                        cluster = ret.stdout.decode().strip().split()[-1][:-1]
                        logurl = 'https://ecallogs.web.cern.ch/'
                        # create task injecting further job info:
                        # - input files
                        # - group: other runs processed by this task
                        # - log file
                        jctrl.createTask(jids=list(range(len(parts))),
                                         recreate=allow_repr,
                                         fields=[{'group' : ','.join([r['run_number'] for r in group[:-1]]),
                                                  'inputs' : infiles,
                                                  'part' : ','.join([str(p), str(sp)]),
                                                  'log' : f'{logurl}/{self.task}-{cluster}-{p}-{sp}.log'} for p,sp in parts])
                        # set the master run to status processing
                        self.rctrl.updateStatus(run=run['run_number'], status={self.task : 'processing'})
                        if len(group) > 1:
                            # set status merged for all other runs.
                            for r in group[:-1]:
                                self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'merged'})
                    else:
                        self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                        return -1
        return 0

    def resubmit(self):
        """
        Query for failed jobs and resubmit.
        Note: condor logs are overwritten.
        """

        runs = self.rctrl.getRuns(status = {self.task : 'processing'})

        for run_dict in runs:
            run = run_dict['run_number']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run, 'fill' : run_dict['fill']},
                            dbname=self.opts.dbname)
            # check for evicted jobs. Jobs still marked as running in the db but not
            # acutally running in condor.
            for jid in jctrl.getRunning()+jctrl.getIdle():
                if not self.check_running_job(jid = jctrl.getJob(jid=jid, last=True)[-1]['htc-id']):
                    jctrl.failed(jid=jid)

            # resubmit failed
            if not jctrl.taskExist() or jctrl.getFailed() == []:
                self.log.info(f'no failures found for run {run}')
            else:
                self.log.info(f'found failed job for run {run}')
                # Bulding the resubmission argumets
                # jobid, files
                # where jobid is the jobid of the original submission
                eosdir = path.abspath(self.opts.eosdir+f'/{run}/')
                flist = jctrl.getJob(0, last=True)[0]['inputs']
                task = f'-w {self.task} -c {self.campaign} -t run_number:{run},fill:{run_dict["fill"]} --db {self.opts.dbname}'
                parts = [str(i)+', '+jctrl.getJob(i, last=True)[0]['part'] for i in jctrl.getFailed()]
                with open('args-align.txt', 'w') as ff:
                    ff.write('\n'.join(parts))

                ret = subprocess.run(['condor_submit',
                                      f'{self.opts.template}',
                                      '-spool',
                                      '-queue', 'procid, part, subpart from args.txt',
                                      '-append', f'arguments = "$(ClusterId).$(ProcId) $(procid) {flist} \'{task}\' {eosdir} {self.opts.wdir} $(part) $(subpart)"',
                                      '-append', f'output = /eos/home-e/ecalgit/www/logs/{self.task}-$(ClusterId)-$(part)-$(subpart).log',
                                      '-append', f'error = /eos/home-e/ecalgit/www/logs/{self.task}-$(ClusterId)-$(part)-$(subpart).log',
                                      '-append', f'+JobFlavour = "{self.opts.resubflv}"'],
                                     capture_output=True)
                #remove('args.txt')
                if ret.returncode == 0:
                    # awfully parse condor_submit output to get the clusterId
                    self.log.info(ret.stdout.decode().strip())
                    # set the job status to idle
                    for i in jctrl.getFailed():
                        jctrl.idle(jid=i)
                else:
                    self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                    return -1
        return 0

    
if __name__ == '__main__':
    handler = HTCHandlerAlignment(task='alignment-align',
                                  prev_input='alignment-reco')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerAlignment)
    
