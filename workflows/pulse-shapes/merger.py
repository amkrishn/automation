#!/usr/bin/env python3
import sys
import os
import shutil
import subprocess
import numpy as np
from multiprocessing import Pool
from typing import Optional, Dict, List, Any
from os import remove, path
from ecalautoctrl import HandlerBase, JobCtrl, process_by_run, prev_task_data_source
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

class PSMergeHandler(HandlerBase):
    """
    Run all steps of the pulse shapes measurement.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        if not deps_tasks:
            deps_tasks = [prev_input]
        else:
            deps_tasks.append(prev_input)
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.isfill = False
        self.prev_input = prev_input
        
        # custom options
        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')

        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')        
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')

    def merge_files(self, files: List[str], outfile: str):
        """
        Merge txt files from pulse shape reco.

        :param files: list of files to merge.
        :param outfile: output file name.        
        """

        data = np.array([np.genfromtxt(f) for f in files])
        weights = data[:,:,5].reshape(data[:,:,5].shape[0], data[:,:,5].shape[1], 1)
        ps = data[:,:,6:]
        # compute the average PS
        avg_ps = np.sum(ps*weights, axis=0)/np.sum(weights, axis=0)
        out = np.concatenate([data[0][:,:5], np.sum(weights, axis=0), avg_ps], axis=1)
        # save output
        np.savetxt(outfile, out, fmt=['%.0d']*6+['%.6f']*12)                            

    def validation_plots(self, new: str=None, ref: str='ref.txt', subdir: str=None) -> str:
        """
        Process the validation plots.

        :param new: new pulse shapes txt file path.
        :param ref: ref pulse shapes txt file path.
        :param subdir: output plots subdirectory (run/fill number).
        """

        plotsurl = f'/pulse-shapes/{subdir}/'
        plotsurl = self.opts.plotsurl+plotsurl        
        if new and ref:
            os.makedirs(path.abspath(self.opts.eosplots+f'/pulse-shapes/{subdir}/'), exist_ok=True)
            plotsopts = ['--do1Ddiff', '--do2Ddiff', '--do2DShapeDiff', '--doNhits']
            for partition in ['EB','EE']:
                exe = os.environ['CMSSW_BASE']+'/src/EcalReconstruction/EcalTools/macro/TagValidation.py'
                cmd = ['python3', path.abspath(exe), '-p',
                       partition, new, ref,
                       '--pdir', path.abspath(self.opts.eosplots+f'/pulse-shapes/{subdir}/'), '--print', 'png,root']
                cmd.extend(plotsopts)
                self.log.info(' '.join(cmd))
                ret = subprocess.run(cmd, capture_output=True)
                if ret.returncode == 0:
                    self.log.info(ret.stdout.decode().strip())
                else:
                    self.log.info(ret.stdout.decode().strip())
                    self.log.info(ret.stderr.decode().strip())
                    self.log.error(f'Failed processing validation plots')
                    return None
            return plotsurl
        else:
            return None
        
    def process_data(self, groups: List[Dict], resubmit: bool=False):
        """
        Execute the merging and plotting step for a single run.
        The merging step compute average pulse shapes weighting each
        input file by the number of hits (separately for each channel).

        :param groups: groups of run to be processed.
        :param resubmit: resubmission of failed jobs.
        """
        
        for group in groups:
            self.log.info('Processing run(s): '+','.join([r['run_number'] for r in group]))
            # reset output
            outputs = {}
            # gather master run details
            mrun = group[-1]
            run = mrun['run_number']
            fill = mrun['fill']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run, 'fill' : mrun['fill']},
                            dbname=self.opts.dbname)
            if not jctrl.taskExist():                
                jctrl.createTask(jids=[0],
                                 fields=[{'group' : ','.join([r['run_number'] for r in group[:-1]])}])
            elif resubmit:
                self.isfill = ('group' in jctrl.getJob(jid=0, last=True)[-1]) and len(jctrl.getJob(jid=0, last=True)[-1]['group'].split(','))>0
            try:
                # set run status
                self.rctrl.updateStatus(run=run, status={self.task : 'processing'})
                for r in group[:-1]:
                    self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'merged'})
                jctrl.running(jid=0)
                # get the data from the referece IOV (the one in the prompt tag)
                ref_file='ref.txt'
                ret = subprocess.run(['./fetch_last_ps_iov.sh', run], capture_output=True)
                if ret.returncode == 0:
                    self.log.info(ret.stdout.decode().strip())
                else:
                    self.log.info(ret.stdout.decode().strip())
                    self.log.info(ret.stderr.decode().strip())
                    self.log.error(f'Failed dumping reference IOV')
                    return None

                # get files
                files = self.get_files(group)
                self.log.info(files)
                # set ouput name
                current_file = path.abspath(path.dirname(list(files)[0])+(f'/template_histograms_ECAL_fill{fill}.txt' if self.isfill else '/template_histograms_ECAL.txt'))
                # merge single job / single runs out files
                self.merge_files(files=files, outfile=current_file)
                # run validation plots
                plotsurl = ''
                if self.opts.eosplots and self.opts.plotsurl:
                    subdir = 'fills/'+fill if self.isfill else 'runs/'+run
                    plotsurl = self.validation_plots(new=current_file, ref=ref_file, subdir=subdir)
                    if not plotsurl:
                        jctrl.failed(jid=0)
                        self.log.error(f'Failed validation plots production {run}')
                        return

                # produce sqlite file
                if self.isfill:
                    ret = subprocess.run([f'{os.environ["CMSSW_BASE"]}/src/EcalReconstruction/dbscripts/stripTxtFile.sh', current_file],
                                         capture_output=True)

                    ret = subprocess.run(['cmsRun',
                                          f'{os.environ["CMSSW_BASE"]}/src/EcalReconstruction/dbscripts/Ecal_PulseShapes_argv_cfg.py',
                                          run,
                                          os.path.basename(current_file)],
                                         capture_output=True)
                
                    eospath = os.path.abspath(os.path.dirname(current_file)+'/'+
                                              f'ecaltemplates_popcon_fill_{fill}.db')
                    shutil.copy(f'ecaltemplates_popcon_run_{run}.db', eospath)
                    outputs['sqlite'] = eospath
                
                # mark as completed
                outputs.update({'output' : current_file, 'plots' : plotsurl})
                jctrl.done(jid=0, fields=outputs)
            
            except Exception as e:
                jctrl.failed(jid=0)
                self.log.error(f'Failed processing merging step for run {run}: {e}')
        
    def submit(self):
        """
        Execute the local merging step. 
        Reprocess re-injected runs as well.
        """

        # process new runs / fill
        self.process_data(groups=self.groups())
            
        return 0
            
    def resubmit(self):
        """
        Resubmit runs with failed jobs
        """

        # get runs in status processing
        runs = self.rctrl.getRuns(status = {self.task : 'processing'})

        # check if any job failed before resubmitting
        for run_dict in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run_dict['run_number'],
                                  'fill' : run_dict['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed())>0:
                self.process_data(groups=[[run_dict]], resubmit=True)
        
        return 0

@prev_task_data_source
@process_by_run
class PSMergeHandlerRun(PSMergeHandler):
    """
    Run the PS production and validation task per run.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=[],
                 **kwargs):        
        super().__init__(task=task,
                         prev_input=prev_input,
                         deps_tasks=deps_tasks,
                         **kwargs)
    
if __name__ == '__main__':
    handler = PSMergeHandlerRun(task='pulse-shapes-merge',
                                prev_input='pulse-shapes')

    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(PSMergeHandlerRun)
