#!/usr/bin/env python3
import sys
import os
import shutil
import subprocess
from typing import List, Optional
from ecalautoctrl import HTCHandler, process_by_fill, dbs_data_source
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

@dbs_data_source
@process_by_fill(fill_complete=True)
class PSValidationHandler(HTCHandler):
    """
    Run the HLT rate validation, either with reference prompt conditions or
    with the pulse shape payload under test.
    
    :param task: task name. The "ref" or "val" suffix will be automatically
    appended based on the value of the `--ref` command line option.
    :param deps_tasks: list of workfow dependencies.
    :param dsetname: input RAW dataset name.
    """

    def __init__(self,
                 task: str,
                 deps_tasks: List[str]=None,
                 dsetname: str=None,
                 **kwargs):        
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)

        self.dsetname = dsetname
        
        self.parser.add_argument('--ref',
                                 dest='refcond',
                                 default=False,
                                 action='store_true',
                                 help='Run with reference conditions if set, validation one otherwise')

        
    def __call__(self):
        """
        Execute the command specified by the command line options.
        A suffix is appended to the task name based on the `--ref` option.
        """

        # parse the cmd line to adjust the task name
        self.opts = self.parser.parse_args()
        self.task += 'ref' if self.opts.refcond else 'val'

        # standard call
        return super().__call__()

if __name__ == '__main__':
    handler = PSValidationHandler(task='pulse-shapes-hlt',
                                  deps_tasks=['pulse-shapes-merge-fill'],
                                  dsetname=['/HLTPhysics*/*/RAW', '/HIHLTPhysics*/*/RAW'])

    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(PSValidationHandler)
