import FWCore.ParameterSet.Config as cms

from ecalautoctrl import RunCtrl, JobCtrl
import argparse

class SplitDict(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, {t.split(':')[0] : t.split(':')[1] for t in values.split(',')})

def customize_ps_tag(process, taskstr: str=None, inpt_wflow: str=None):
    """
    Customize the CMSSW process replacing the default GT pulse shapes with
    the tag under test.

    :param process: the CMSSW process.
    :param taskstr: a string containing the option that identify the ongoing job.
    :param inpt_wflow: name of the workflow from which to read the new payload.
    """

    # suppress errors
    #process.MessageLogger.cerr.FwkSummary.limit = 1
    process.MessageLogger.cerr.FwkReport.limit = 1
    
    # parse task string
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--campaign', dest='campaign',
                        help='ECAL processing campaign', required=True)
    parser.add_argument('-w', '--workflow', dest='workflow',
                        help='ECAL workflow', required=True)
    parser.add_argument('-t', '--tags', dest='tags', default='',
                        action=SplitDict, help='User defined tags')
    parser.add_argument('--db', dest='dbname', default='ecal_offline_test',
                        type=str, help='Specify to which db write the data')

    opts = parser.parse_args(taskstr.split())

    # change tag only if the workflow is marked with the suffix "val".
    if opts.workflow[-3:] == 'val':
        # fetch the sqlite file name
        rctrl = RunCtrl(dbname=opts.dbname, campaign=opts.campaign)
        # assuming one and only one output per fill
        sqlite = list(rctrl.getField(process=opts.workflow.replace('hltval', 'merge-fill'),
                                     field='sqlite',
                                     fills=[opts.tags['fill']]))
        assert(len(sqlite)==1)
        sqlite = sqlite[-1]

        process.GlobalTag.toGet = cms.VPSet(
            cms.PSet(record = cms.string('EcalPulseShapesRcd'),
                     tag = cms.string('EcalPulseShapes_data'),
                     connect = cms.string(f'sqlite_file:{sqlite}')))    
