#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

sleep 10

mkdir output

RUN=`echo $TASK | sed -e 's/^.*run_number://g' | sed -e 's/,.*$//g'`

source customizeHltGetConfiguration.sh
./hltGetConfigurationECAL run:$RUN --output none --eras Run3 --max-events=-1 --taskstr "\"$TASK\"" --globaltag "" --dumpname=output/hlt.py

cmsRun output/hlt.py inputFiles=$INFILE
RET=$?

pathToMonitor=("HLT_Ele32_WPTight_Gsf" "HLT_Ele35_WPTight_Gsf" "HLT_Ele38_WPTight_Gsf" "HLT_Ele30_eta2p1_WPTight_Gsf_CentralPFJet35_EleCleaned" "HLT_Photon33" "HLT_PFMET120_PFMHT120_IDTight" "HLT_HIEle20Gsf" "HLT_HIGEDPhoton30")

if [ "$RET" == "0" ]
then
    # gather trigger rates
    INFO=""
    for TRG in ${pathToMonitor[*]}
    do
        RATE=`cat _condor_stdout | grep ${TRG}_v | grep 'TrigReport' | grep -v '\-----' |  awk '{if ($5 != 0) print $5*2000/$4}'`
        INFO=$INFO" ${TRG}:${RATE}"
    done
    # write the trigger-path to trigger-rate map into influxdb as custom fields
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields $INFO
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
