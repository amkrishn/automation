#!/usr/bin/env python3
import sys
import logging
import subprocess
import json
from typing import Optional, List
from ecalautoctrl import HandlerBase, JobCtrl, process_by_fill
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

@process_by_fill(fill_complete=True)
class ConddbUploaderHandler(HandlerBase):
    """
    Run all steps of the pulse shapes measurement.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 db_prod_step: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=[db_prod_step], **kwargs)
        self.db_prod_step = db_prod_step

        # custom options
        self.submit_parser.add_argument('--dest-tags',
                                        dest='dest_tags',
                                        default=None,
                                        type=str,
                                        help='Comma separated list of tags to update')
        self.submit_parser.add_argument('--oracle-db',
                                        dest='oracle_db',
                                        default='oracle://cms_orcoff_prep/CMS_CONDITIONS',
                                        type=str,
                                        help='URL to oracle db: prep or prod')

    def submit(self):
        """Submit upload step."""

        for group in self.groups():
            mrun = group[-1]
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': mrun['run_number'],
                                  'fill': mrun['fill']},
                            dbname=self.opts.dbname)
            # set run/job status
            if not jctrl.taskExist():
                jctrl.createTask(jids=[0],
                                 fields=[{'group': ','.join([r['run_number'] for r in group[:-1]])}])
            self.rctrl.updateStatus(run=mrun['run_number'],
                                    status={self.task: 'processing'})
            for r in group[:-1]:
                self.rctrl.updateStatus(run=r['run_number'],
                                        status={self.task: 'merged'})
            jctrl.running(jid=0)

            try:
                sqlite = self.rctrl.getField(field='sqlite',
                                             process=self.db_prod_step,
                                             runs=[mrun['run_number']])
                sqlite = list(sqlite)[-1]
                # generate metadata for conddb upload
                metadata = {
                    'destinationDatabase': self.opts.oracle_db,
                    'destinationTags': {t: {} for t in self.opts.dest_tags.split(',')},
                    'inputTag': 'EcalPulseShapes_data',
                    'since': int(mrun['run_number']),
                    'userText': f'ECAL automation upload for fill {mrun["fill"]}',
                }

                metadata = json.dumps(metadata, sort_keys=True, indent=4)
                logging.info('\nThis is the generated metadata:\n%s' % metadata)
                with open(sqlite[:-2]+'txt', 'w') as fmeta:
                    fmeta.write(metadata)

                # conddb upload with the CMSSW tool
                logging.info(f'Uploading with: uploadConditions.py {sqlite}')
                ret = subprocess.run(['uploadConditions.py', sqlite], capture_output=True)
                if ret.returncode == 0:
                    self.log.info(ret.stdout.decode().strip())
                    jctrl.done(jid=0)
                else:
                    self.log.info(ret.stdout.decode().strip())
                    self.log.info(ret.stderr.decode().strip())
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed uploading payload to conddb for fill {mrun["fill"]}')

            except Exception as e:
                jctrl.failed(jid=0)
                self.log.error(f'Failed uploading payload to conddb for fill {mrun["fill"]}: {e}')

        return 0

    
if __name__ == '__main__':
    handler = ConddbUploaderHandler(task='pulse-shapes-upload',
                                    db_prod_step='pulse-shapes-merge-fill')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(ConddbUploaderHandler)
