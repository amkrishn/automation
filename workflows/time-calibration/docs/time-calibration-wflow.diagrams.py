from diagrams import Diagram, Cluster, Node
from diagrams.aws.compute import EC2Instance as singleJob
from diagrams.aws.compute import EC2Instances as multiJob
from diagrams.aws.management import OpsworksPermissions as locks
from diagrams.aws.iot import IotAnalyticsDataSet as file

with Diagram("Worflow: time calibration", filename="time-calibration-wflow", show=False, direction="TB") as d:
    # s = singleJob("Single job", width='0.5')
    # m = multiJob("Batch jobs", width='0.5')
    # f = file("Workflow related files", width='0.5')
    with Cluster("timing-reco", direction="LR"):
        Node("", width='1.', height='0', style='invisible')
        t0_lock = locks("T0ProcDatasetLock \n dataset=/AlCaPhiSym \n stage=Repack", width='1.6')
        reco_files = file("runreco.py \n template.sub \n batch_script.sh")
        phisym_reco = multiJob("HTCHandlerByRunDBS \n dsetname=/AlCaPhiSym/*/RAW")

    t0_lock >> phisym_reco

with Diagram("Worflow: time calibration CC timing", filename="time-calibration-cc-wflow", show=False, direction="TB") as d_cc:
    # s = singleJob("Single job", width='0.5')
    # m = multiJob("Batch jobs", width='0.5')
    # f = file("Workflow related files", width='0.5')
    with Cluster("timing-reco-cc", direction="LR"):
        Node("", width='1.', height='0', style='invisible')
        t0_lock = locks("T0ProcDatasetLock \n dataset=/AlCaPhiSym \n stage=Repack", width='1.6')
        reco_files = file("runreco_cc.py \n template_cc.sub \n batch_script_cc.sh")
        phisym_reco = multiJob("HTCHandlerByRunDBS \n dsetname=/AlCaPhiSym/*/RAW")

    t0_lock >> phisym_reco
