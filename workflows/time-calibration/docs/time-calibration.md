# Time calibration

## Resources
Quick links:

- [Jenkins job](https://dpg-ecal-calib.web.cern.ch/view/ECAL%20Prompt/job/timing-prod/) 
- [Calibration code](https://github.com/simonepigazzini/EcalTiming/tree/automation)
- [Automation config](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/tree/master/workflows/time-calibration)

## Workflow structure
The time calibration runs on a dedicated stream that stores only ECAL digis from ZeroBias events. The stream is saved in a RAW dataset:

- `/AlCaPhiSym/*/RAW`

The first step of the calibration runs the reconstruction in two different versions. On version uses the RatioMethod timing reconstruction algorithm and the other one uses the crossCorrelationMethod timing algorithm and tighter kOutOfTime thresholds in EB.

The automation workflow structure can be seen in the diagram below.

![wflow-fig](time-calibration-wflow.png)
![wflow-fig](time-calibration-cc-wflow.png)




