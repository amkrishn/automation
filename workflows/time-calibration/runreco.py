#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandlerByRunDBS, T0ProcDatasetLock
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

if __name__ == '__main__':
    t0lock = T0ProcDatasetLock(dataset='/AlCaPhiSym', stage='Repack')

    handler = HTCHandlerByRunDBS(task='timing-reco',
                                 dsetname='/AlCaPhiSym/*/RAW',
                                 locks=[t0lock])

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerByRunDBS)
