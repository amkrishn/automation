#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandlerByRunDBS, T0ProcDatasetLock
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

if __name__ == '__main__':
    t0lock0 = T0ProcDatasetLock(dataset='/EGamma0', stage='PromptReco')
    t0lock1 = T0ProcDatasetLock(dataset='/EGamma1', stage='PromptReco')

    handler = HTCHandlerByRunDBS(task='ecalelf-ntuples-zskim',
                                 dsetname="/EGamma*/*EcalUncalZElectron-Prompt*/ALCARECO",
                                 locks=[t0lock0, t0lock1])    
    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerByRunDBS)
