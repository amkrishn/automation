#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

mkdir output
cmsRun $WDIR/src/CalibCode/submit/monitoring/reconstructPi0_template.py outputDir=output/ outputFile=AlCaPi0_$JOBID.root inputFiles=$INFILE conditionsFileName='GTconditions_withCalib_cff' globalTag=$GT
RETCMSSW=$?

if [ "$RETCMSSW" == "0" ]
then
    mkdir -p $EOSDIR
    cp output/*root $EOSDIR
    OFILE=`ls output/*root`
    OFILE=$EOSDIR/`basename $OFILE`
    RETCOPY=$?
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
