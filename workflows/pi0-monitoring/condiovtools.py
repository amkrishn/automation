#!/usr/bin/env python3
import subprocess
from datetime import datetime

def get_cond_iov_runs(tag, greptype):
    """
    Get the runs corresponding to the IOVs of a tag.

    :param tag: Tag name
    :param greptype: Tag type name (or fragment of it) for to select relevant lines in conddb list output
    :return: List of integer run numbers
    """
    ret = subprocess.run("conddb list -L 250 " + tag + " 2> /dev/null | grep '" + greptype + "' | awk '{print $1}'", capture_output=True, shell=True)
    ret = ret.stdout.decode('utf-8').strip()
    iovs = [l.split()[0] for l in ret.split('\n')]
    iovruns = [int(i) for i in iovs]
    return iovruns

def get_closest_run_info(run, omsquery, omsapi):
    """
    Get the information of the closest run lower than the one runs given for which OMS has information.

    :param run: Run number
    :param omsquery: automation control QueryOMS object
    :param omsapi: OMSAPI object
    :return: Dictionary with information about the closest run
    """
    query = omsapi.query('runs')
    query.sort('run_number', asc=False)
    query.paginate(page=1, per_page=1)
    query.filter('run_number', int(run), 'LT') 
    query.attrs(['run_number', 'end_time'])
    results = omsquery.get_oms_data(query, daq_completed=True)
    return results

def convert_iso8601_to_timestamp(timestr):
    """
    Convert ISO 8601 time string from OMS to timestamp.

    :param timestr: ISO 8601 time string
    :return: timestamp
    """
    # python < 3.11 does not understand the 'Z' in the time string from OMS
    return int(datetime.fromisoformat(timestr.replace("Z", "+00:00")).timestamp())

def get_run_info(runs, omsquery, omsapi):
    """
    Get the start times of the runs from OMS. If there is no information for a run the end time of the closest previous run for with OMS has information is used instead. In this case the returned run nunmber is still the one from the input run list.

    :param runs: List of run numbers
    :param omsquery: automation control QueryOMS object
    :param omsapi: OMSAPI object
    :return: List of run numbers as strings
    :return: List of run start (or end) times (UTC)
    """
    run_info = omsquery.get_runs(runs=runs)
    runstrs = []
    run_start_times = []
    for run in runs:
        if run in run_info and run_info[run]["start_time"] != None:
            runstrs.append(str(run))
            run_start_times.append(convert_iso8601_to_timestamp(run_info[run]["start_time"]))
        else:
            print("No information about run {} found in OMS.".format(run))
            closest_run_info = get_closest_run_info(run, omsquery, omsapi)
            closest_runs = list(closest_run_info)
            if len(closest_runs) > 0:
                closest_run = closest_runs[0]
                print("Using end time of closest previous run {} from OMS, instead.".format(closest_run))
                runstrs.append(str(run))  # still print the actual IOV run
                run_start_times.append(convert_iso8601_to_timestamp(closest_run_info[closest_run]["end_time"]))
    return runstrs, run_start_times

def merge_iovruns(runlists, typestrs):
    """
    Merge runs for which several conditions have updated toegther.

    :param runlists: List of list of runs for which tags were updated
    :param typestrs: Tag type strings to be printed on the plot when a tag was updated
    :return: Dictionary with tag type strings updated for a given run
    """
    runcondstrs = {}
    for runs, typestr in zip(runlists, typestrs):
        runs = list(set(runs))
        for run in runs:
            if run not in runcondstrs:
                runcondstrs[run] = typestr
            else:
                runcondstrs[run] += '+' + typestr
    return runcondstrs

