Automation workflow docs: ECAL Pi0 monitoring
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

runreco.py
==========

.. argparse::
   :filename: ../runreco.py
   :func: get_opts
   :prog: runreco.py

runmon.py
=========

.. argparse::
   :filename: ../runmon.py
   :func: get_opts
   :prog: runmon.py

runmoncml.py
============

.. argparse::
   :filename: ../runmoncml.py
   :func: get_opts
   :prog: runmoncml.py
      
