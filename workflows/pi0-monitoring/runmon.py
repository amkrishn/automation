#!/usr/bin/env python3
import sys
import os
import subprocess
from importlib import util
# import ROOT if possible (use mock import for docs generation)
try:
    if util.find_spec('ROOT'):
        import ROOT
except:
    from unittest.mock import MagicMock, patch
    my_conddb = MagicMock()
    patch.dict("sys.modules", fake_root=my_conddb).start()
    import fake_root as ROOT
from typing import Optional, List
from ecalautoctrl import JobCtrl, HandlerBase, prev_task_data_source, process_by_fill
from ecalautoctrl.CMSTools import QueryOMS
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase
from omsapi import OMSAPI
from condiovtools import *

@prev_task_data_source
@process_by_fill(fill_complete=True)
class Pi0MonHandler(HandlerBase):
    """
    Execute all the steps to generate the pi0 monitoring plots.
    Process fills that have been dumped (completed).

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')

        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')
        
    def resubmit(self):
        """
        Mark failed run for reprocessing.
        """

        # get the new run to process
        runs = self.rctrl.getRuns(status = {self.task : 'processing'})
        # check if any job failed, if so mark all runs for reprocessing
        for run_dict in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run_dict['run_number'],
                                  'fill' : run_dict['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed())>0:
                self.rctrl.updateStatus(run=run_dict['run_number'], status={self.task : 'reprocess'})
                group = jctrl.getJob(jid=0, last=True)[-1]['group'] if 'group' in jctrl.getJob(jid=0, last=True)[-1] else ''
                if group:
                    for r in group.split(','):
                        self.rctrl.updateStatus(run=r, status={self.task : 'reprocess'}) 
                

    def submit(self):
        """
        Read the reco files and produce the monitoring plots.
        This submit step is meant to be run interactively.
        """

        macro = './finalTimeVariationPlot.C'
        ROOT.gROOT.LoadMacro(macro)

        omsapi = OMSAPI("https://cmsoms.cern.ch/agg/api", "v1", cert_verify=False, verbose=False)
        omsapi.auth_oidc('ecalgit-omsapi', 'c5efaa2d-5900-4451-875e-ced3fa57b790')
        omsquery = QueryOMS()

        for group in self.groups():
            # master run
            run = group[-1]
            fdict = self.get_files(group)
            if fdict is not None and len(fdict)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number' : run['run_number'], 'fill' : run['fill']},
                                dbname=self.opts.dbname)
                if not jctrl.taskExist():
                    jctrl.createTask(jids=[0],
                                     fields=[{'group' : ','.join([r['run_number'] for r in group[:-1]])}])
                try:
                    jctrl.running(jid=0)
                    self.log.info(f'Processing fill {run["fill"]}.')
                    self.rctrl.updateStatus(run=run['run_number'], status={self.task : 'processing'})
                    for r in group[:-1]:
                        self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'merged'})
                    eosdir = os.path.abspath(self.opts.eosplots+f'/{run["fill"]}/')
                    plotsurl = self.opts.plotsurl+f'/{run["fill"]}/'
                    os.makedirs(eosdir, exist_ok=True)
                    # run the splitting and mass fits 
                    # exe = os.environ['CMSSW_BASE']+'/src/CalibCode/submit/monitoring/collectEventInTimeFrame.py'
                    exe = 'collectEventInTimeFrame.py'
                    cmd = ['python3', os.path.abspath(exe), '--inFile', ','.join(fdict),
                           '--outFile', os.path.abspath(f'{eosdir}/pi0_fitMassInfo_{run["fill"]}.txt')]
                    self.log.info(' '.join(cmd))
                    ret = subprocess.run(cmd, capture_output=True)
                    if ret.returncode == 0:
                        self.log.info(ret.stdout.decode().strip())
                    else:
                        jctrl.failed(jid=0)
                        self.log.info(ret.stdout.decode().strip())
                        self.log.info(ret.stderr.decode().strip())
                        self.log.error(f'Failed processing collectEventInTimeFrame.py')
                        return None

                    with open("timeVariationFileList.txt", "w") as finalin:
                        finalin.write('#TextFile,Label,Color\n')
                        finalin.write(f'{eosdir}/pi0_fitMassInfo_{run["fill"]}.txt,With Light Monitoring Correction,417')

                    # tags for which IOV updates are shown in the plot
                    psiovruns = get_cond_iov_runs("EcalPulseShapes_prompt", "EcalCond")
                    iciovruns = get_cond_iov_runs("EcalIntercalibConstants_V1_prompt", "EcalCond")
                    timeiovruns = get_cond_iov_runs("EcalTimeCalibConstants_v01_prompt", "EcalCond")
                    ebaligniovruns = get_cond_iov_runs("EBAlignment_measured_v01_express", "Alignments")
                    eealigniovruns = get_cond_iov_runs("EEAlignment_measured_v02_express", "Alignments")
                    esaligniovruns = get_cond_iov_runs("ESAlignment_measured_v01_express", "Alignments")
                    pediovruns = get_cond_iov_runs("EcalPedestals_prompt", "EcalCond")

                    tagiovruns = [psiovruns, timeiovruns, iciovruns, ebaligniovruns, eealigniovruns, esaligniovruns, pediovruns]
                    taglabels = ["PS", "T", "IC", "A^{EB}", "A^{EE}", "A^{ES}", "P"]

                    runcondstrs = merge_iovruns(tagiovruns, taglabels)
                    iovruns = [r for r in sorted(runcondstrs) if r > 347000]
                    runstrs, run_start_times = get_run_info(iovruns, omsquery, omsapi)
                    condstrs = ["{0}-{1}".format(runcondstrs[int(r)], r) for r in runstrs]

                    ROOT.finalTimeVariationPlot("timeVariationFileList.txt", eosdir+'/', False, condstrs, run_start_times)

                    # mark as completed
                    jctrl.done(jid=0, fields={
                        'output' : f'{eosdir}/pi0_fitMassInfo_{run["fill"]}.txt',
                        'plots' : plotsurl})

                except Exception as e:
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed producing the monitoring plots for fill {run["fill"]}: {e}')
                    continue
    
if __name__ == '__main__':
    handler = Pi0MonHandler(task='pi0-mon',
                            deps_tasks=['pi0-reco'],
                            prev_input='pi0-reco')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(Pi0MonHandler)
    
