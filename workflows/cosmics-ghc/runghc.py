#!/usr/bin/env python3
import argparse
import requests
import logging
import sys
from ecalautoctrl import RunCtrl, QueryDBS, JobCtrl, HandlerBase
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

class GHCJobHandler(HandlerBase):
    """
    Job interface class. Inherits the cmd line options from the ecalautoctrl base class.

    :param appname: name for logging purposes
    """
    def __init__(self, task: str):
        super().__init__(task=task)        
        
        # add task specific options
        self.parser.add_argument('--pd',
                                 dest='dataset',
                                 default='ExpressCosmics',
                                 type=str,
                                 help='Primary dataset')
        self.parser.add_argument('--dtire',
                                 dest='datatire',
                                 default='FEVT',
                                 type=str,
                                 help='Data tier')
        
    def submit(self):
        """Good health check of the system. Fetch new runs, collect data from dbs."""        
        self.mm_handler.setLevel(logging.ERROR)
    
        runs = self.rctrl.getRuns(status = {self.task : 'new'})
        runs.extend(self.rctrl.getRuns(status = {self.task : 'processing'})) # include
        runs.extend(self.rctrl.getRuns(status = {self.task : 'reprocess'}))
        
        if not len(runs):
            self.log.info('No runs to process')
            return 0

        for run in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run['run_number'],
                                  'fill' : run['fill']},
                            dbname=self.opts.dbname)
            if not jctrl.taskExist() or len(jctrl.getFailed())>0 or run[self.task]=='reprocess':
                if not jctrl.taskExist():
                    jctrl.createTask(jids=[0])
        
                jctrl.running(jid=0)
                self.rctrl.updateStatus(run=run['run_number'], status={self.task : 'processing'})
                try:
                    self.log.info('Getting files for run %s, dataset %s:',
                                  run['run_number'],
                                  f'/{self.opts.dataset}*/*{run["era"]}*/{self.opts.datatire}')

                    dataQuery = QueryDBS(dataset=f'/{self.opts.dataset}*/*{run["era"]}*/{self.opts.datatire}')
                    files = dataQuery.getRunFiles(run['run_number'])
        
                    if len(files):
                        self.log.info('Number of available files: %s', str(len(files)))
                    else:
                        self.log.warning('No file from run %s found in dataset %s',
                                         run['run_number'],
                                         f'/{self.opts.dataset}*/*{run["era"]}*/{self.opts.datatire}')
                    jctrl.done(jid=0)
                except Exception as e:                
                    jctrl.failed(jid=0)
                    self.log.error(f'failed processing run {run["run_number"]}: {e}')

        return 0
                
if __name__ == '__main__':
    ghc = GHCJobHandler(task='trivial-ghc')
    ret = ghc()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(GHCJobHandler)
