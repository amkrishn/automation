# Trivial GHC

## Resources
Quick links:

- [Jenkins job](https://dpg-ecal-calib.web.cern.ch/view/ECAL%20Prompt/job/cosmics-ghc-prod/) 
- [Automation config](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/tree/master/workflows/cosmics-ghc)

## Workflow structure
Simply fetch all new runs acquired in P5 and check that reading Express files works.

!!! Note
    The workflow has `cosmics` in the name simply because this *can* run on cosmics as well as on collision data. There is nothing related to calibration with cosmics here.

